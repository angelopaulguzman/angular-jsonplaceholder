import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  user$:{};
  id: number;

  constructor(private data: DataService, private route: ActivatedRoute ) {
    this.user$={};
    this.id=0;
    this.route.params.subscribe(params => this.id = params.id)
  }
  ngOnInit() {
    this.data.getUser(this.id).subscribe(
      data => this.user$ = data
    )
  }

}
